# Simple Calculator
This project is part of **Pekan Ristek 2019** academy event.

### Overview
This application provides four methods for simple arithmetic operation (addition, subtraction, multiplication, and division).

### Requirements
- Gradle >= 5.6.4
- Java Development Kit (JDK) >= 1.8.0_231

### Build Instruction
- Clone this repository to your local file system using one of the following command:
~~~shell script
git clone git@gitlab.com:khadijah.rizqy/ristek-calculator-mddj.git
~~~
~~~shell script
git clone https://gitlab.com/khadijah.rizqy/ristek-calculator-mddj.git
~~~
- Navigate to this repository's root and run the following command:
~~~shell script
./gradlew build 
~~~

### Run Instruction
- Navigate to this repository's root and run the following command:
~~~shell script
./gradlew run
~~~

### Authors
- [Khadijah Rizqy Mufida](https://gitlab.com/khadijah.rizqy)
- [Rafi Muhammad Daffa](https://gitlab.com/skycruiser8)
